package stack;



public class StackItem {
    private Object item;
    private StackItem next;

    public StackItem(Object item){
        this.item = item;
    }
    public void sNext(StackItem next) {
        this.next = next;
    }

    public StackItem gNext() {
        return next;
    }



    public Object Item() {
        return item;
    }

}