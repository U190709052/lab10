package demo;

import stack.Stack;
import stack.StackImpl;

public class StackDemo {
    public static void main(String[] args) {
        Stack sarp = new StackImpl();
        sarp.push("A");
        sarp.push("B");
        sarp.push("C");
        sarp.push("D");

        while (!sarp.empty()){
            System.out.println(sarp.pop());
        }
    }
}
